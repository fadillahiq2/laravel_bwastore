<?php

use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProductGalleryController;
use App\Http\Controllers\Admin\TransactionContoller;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/categories', [CategoryController::class, 'index'])->name('categories');
Route::get('/categories/{id}', [CategoryController::class, 'detail'])->name('categories.detail');

Route::get('/details/{id}', [DetailController::class, 'index'])->name('detail');
Route::post('/details/{id}', [DetailController::class, 'add'])->name('detail.add');

Route::get('/success', [CartController::class, 'success'])->name('success');
Route::get('/register/success', [RegisterController::class, 'success'])->name('register.success');

Route::middleware(['auth'])->group(function () {
    Route::get('/cart', [CartController::class, 'index'])->name('cart');
    Route::delete('/cart/{id}', [CartController::class, 'delete'])->name('cart.delete');

    Route::post('/checkout', [CheckoutController::class, 'process'])->name('checkout');
    Route::post('/checkout/callback', [CheckoutController::class, 'callback'])->name('checkout.callback');
});

Route::prefix('dashboard')->middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('products', [DashboardController::class, 'products'])->name('dashboard.products');
    Route::get('products/create', [DashboardController::class, 'products_create'])->name('dashboard.products.create');
    Route::post('products/store', [DashboardController::class, 'products_store'])->name('dashboard.products.store');
    Route::get('products/{id}', [DashboardController::class, 'products_details'])->name('dashboard.products.details');
    Route::put('products/{id}', [DashboardController::class, 'products_update'])->name('dashboard.products.update');

    Route::post('products/gallery/upload', [DashboardController::class, 'upload_gallery'])->name('dashboard.products.gallery.upload');
    Route::delete('products/gallery/delete/{id}', [DashboardController::class, 'delete_gallery'])->name('dashboard.products.gallery.delete');

    Route::get('transactions', [DashboardController::class, 'transactions'])->name('dashboard.transactions');
    Route::get('transactions/{id}', [DashboardController::class, 'transactions_details'])->name('dashboard.transactions.details');

    Route::put('transactions/{id}', [DashboardController::class, 'transactions_update'])->name('dashboard.transactions.update');

    Route::get('store/settings', [DashboardController::class, 'store_settings'])->name('dashboard.store.settings');
    Route::get('account/settings', [DashboardController::class, 'account_settings'])->name('dashboard.account.settings');
    Route::post('account/settings/{redirect}', [DashboardController::class, 'account_settings_update'])->name('dashboard.account.update');
});

Route::prefix('admin')->middleware('auth', 'admin')->group(function () {
    Route::get('/', [AdminDashboardController::class, 'index'])->name('admin.dashboard');
    Route::resource('category', AdminCategoryController::class);
    Route::resource('user', UserController::class);
    Route::resource('product', ProductController::class);
    Route::resource('product-gallery', ProductGalleryController::class);
    Route::resource('transaction', TransactionContoller::class);
});

Auth::routes();
