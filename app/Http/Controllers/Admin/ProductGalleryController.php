<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;
use App\Http\Requests\Admin\ProductGalleryRequest;
use App\Models\Category;
use App\Models\ProductGallery;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;

class ProductGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            $query = ProductGallery::with(['product']);

            return Datatables::of($query)
                ->addColumn('action', function($item) {
                return '

                                <form class="d-flex" action="'.route('product-gallery.destroy', $item->id).'" method="POST">
                                '.method_field('DELETE') . csrf_field() .'
                                <a class="btn btn-warning mr-1" href="'.route('product-gallery.edit', $item->id).'">
                                    Edit
                                </a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                ';
            })
            ->editColumn('photos', function($item) {
                return $item->photos ? '<a href="'.asset('photos-gallery/'.$item->photos).'" target="_blank"><img src="'. asset('photos-gallery/'.$item->photos) .'" class="rounded mt-2 mb-2" style="max-height: 80px;" /></a>' : '';
            })
            ->rawColumns(['action', 'photos'])
            ->make();
        }

        return view('pages.admin.product-gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        return view('pages.admin.product-gallery.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'photos' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'products_id' => 'required|exists:products,id'
        ]);

        $imageName = time().rand(100,999).".".$request->photos->getClientOriginalExtension();

        $request->photos->move(public_path().'/photos-gallery', $imageName);
        ProductGallery::create([
            'products_id' => $request->products_id,
            'photos' =>$imageName,
        ]);

        alert()->success('Success','Berhasil menambahkan photo !')->persistent('Ok');
        return redirect()->route('product-gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();
        $product_gallery = ProductGallery::findOrFail($id);
        return view('pages.admin.product-gallery.edit', compact('products', 'product_gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product_gallery = ProductGallery::findOrFail($id);

        $photo = $request->file('photos');

        // Kalo pas diedit gambar diganti / masukin gambar
        if($photo) {
            $this->validate($request, [
                'photos' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
                'products_id' => 'required|exists:products,id'
            ]);

            $imageName = time().rand(100,999).".".$photo->getClientOriginalExtension();

            $photo->move(public_path().'/photos-gallery', $imageName);
            $product_gallery->update([
                'products_id' => $request->products_id,
                'photos' =>$imageName,
            ]);

            alert()->success('Success','Berhasil memperbarui photo !')->persistent('Ok');
            return redirect()->route('product-gallery.index');

        }
            $this->validate($request, [
                'products_id' => 'required|exists:products,id'
            ]);

            $product_gallery->update([
                'products_id' => $request->products_id,
            ]);

            alert()->success('Success','Berhasil memperbarui photo !')->persistent('Ok');
            return redirect()->route('product-gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = ProductGallery::findOrFail($id);
        $photo = public_path('/photos-gallery/').$product->photos;
        if(file_exists($photo))
        {
            @unlink($photo);
        }
        $product->delete();

        alert()->success('Success','Berhasil menghapus product !')->persistent('Ok');
        return redirect()->route('product-gallery.index');
    }
}
