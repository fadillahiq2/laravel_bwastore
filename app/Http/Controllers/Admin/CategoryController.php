<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            $query = Category::query();

            return Datatables::of($query)
                ->addColumn('action', function($item) {
                return '

                                <form class="d-flex" action="'.route('category.destroy', $item->id).'" method="POST">
                                '.method_field('DELETE') . csrf_field() .'
                                <a class="btn btn-warning mr-1" href="'.route('category.edit', $item->id).'">
                                    Edit
                                </a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                ';
            })
            ->editColumn('photo', function($item) {
                return $item->photo ? '<a href="'.asset('photos/'.$item->photo).'" target="_blank"><img src="'. asset('photos/'.$item->photo) .'" class="rounded mt-2 mb-2" style="max-height: 80px;" /></a>' : '';
            })
            ->rawColumns(['action', 'photo'])
            ->make();
        }

        return view('pages.admin.category.index')->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'photo' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $slug = Str::slug($request->name);

        $imageName = time().rand(100,999).".".$request->photo->getClientOriginalExtension();

        $request->photo->move(public_path().'/photos', $imageName);
        Category::create([
            'name' => $request->name,
            'photo' =>$imageName,
            'slug' => $slug
        ]);

        alert()->success('Success','Berhasil menambahkan category !')->persistent('Ok');
        return redirect()->route('category.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('pages.admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $photo = $request->file('photo');

        // Kalo pas diedit gambar diganti / masukin gambar
        if($photo) {
            $this->validate($request, [
                'name' => 'required|string',
                'photo' => 'image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $slug = Str::slug($request->name);

            $imageName = time().rand(100,999).".".$photo->getClientOriginalExtension();

            $photo->move(public_path().'/photos', $imageName);
            $category->update([
                'name' => $request->name,
                'photo' =>$imageName,
                'slug' => $slug
            ]);

            alert()->success('Success','Berhasil memperbarui category !')->persistent('Ok');
            return redirect()->route('category.index');

        }
            $this->validate($request, [
                'name' => 'required|string',
            ]);

            $slug = Str::slug($request->name);

            $category->update([
                'name' => $request->name,
                'slug' => $slug,
            ]);

            alert()->success('Success','Berhasil memperbarui category !')->persistent('Ok');
            return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        alert()->success('Success','Berhasil menghapus category !')->persistent('Ok');
        return redirect()->route('category.index');
    }
}
