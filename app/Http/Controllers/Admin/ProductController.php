<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;
use App\Http\Requests\Admin\ProductRequest;
use App\Models\Category;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            $query = Product::with(['user', 'category']);

            return Datatables::of($query)
                ->addColumn('action', function($item) {
                return '

                                <form class="d-flex" action="'.route('product.destroy', $item->id).'" method="POST">
                                '.method_field('DELETE') . csrf_field() .'
                                <a class="btn btn-warning mr-1" href="'.route('product.edit', $item->id).'">
                                    Edit
                                </a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                ';
            })
            ->rawColumns(['action'])
            ->make();
        }

        return view('pages.admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $categories = Category::all();
        return view('pages.admin.product.create', compact('users', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->all();

        $data['slug'] = Str::slug($request->name);

        Product::create($data);

        alert()->success('Success','Berhasil menambahkan product !')->persistent('Ok');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::all();
        $categories = Category::all();
        $product = Product::findOrFail($id);
        return view('pages.admin.product.edit', compact('product', 'users', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $data = $request->all();

        $data['slug'] = Str::slug($request->name);

        $product->update($data);

        alert()->success('Success','Berhasil memperbarui product !')->persistent('Ok');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        alert()->success('Success','Berhasil menghapus product !')->persistent('Ok');
        return redirect()->route('product.index');
    }
}
