<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;
use App\Http\Requests\Admin\UserRequest;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
        {
            $query = User::query();

            return Datatables::of($query)
                ->addColumn('action', function($item) {
                return '

                                <form class="d-flex" action="'.route('user.destroy', $item->id).'" method="POST">
                                '.method_field('DELETE') . csrf_field() .'
                                <a class="btn btn-warning mr-1" href="'.route('user.edit', $item->id).'">
                                    Edit
                                </a>
                                <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                ';
            })
            ->rawColumns(['action'])
            ->make();
        }

        return view('pages.admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users'
        ]);

        $data = $request->all();

        $data['password'] = bcrypt($request->password);

        User::create($data);

        alert()->success('Success','Berhasil menambahkan user !')->persistent('Ok');
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('pages.admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {

        $data = $request->all();
        $user = User::findOrFail($id);
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        if($request->password)
        {
            $data['password'] = bcrypt('password');
        }else
        {
            unset($data['password']);
        }

        $user->update($data);

        alert()->success('Success','Berhasil memperbarui user !')->persistent('Ok');
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        alert()->success('Success','Berhasil menghapus user !')->persistent('Ok');
        return redirect()->route('user.index');
    }
}
