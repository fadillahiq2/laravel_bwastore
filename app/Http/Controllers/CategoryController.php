<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
class CategoryController extends Controller
{
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        Paginator::useBootstrap();
        $categories = Category::all();
        $products = Product::with('galleries')->latest()->paginate(12);


        return view('pages.category', compact('categories', 'products'))->with('i')->with('p');
    }

    public function detail(Request $request, $slug)
    {
        Paginator::useBootstrap();
        $categories = Category::all();
        $category = Category::where('slug', $slug)->firstOrFail();
        $products = Product::with('galleries')->where('categories_id', $category->id)->paginate(12);


        return view('pages.category', compact('categories', 'products'))->with('i')->with('p');
    }
}
