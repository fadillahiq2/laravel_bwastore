<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\TransactionDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    public function index()
    {
        $transactions = TransactionDetail::with(['transaction.user', 'product.galleries'])->whereHas('product', function($product){
            $product->where('users_id', Auth::user()->id);
        });

        $revenue = $transactions->get()->reduce(function ($carry, $item) {
            return $carry + $item->price;
        });

        $customer = User::count();
        return view('pages.dashboard', [
            'transaction_count' => $transactions->count(),
            'transaction_data' => $transactions->get(),
            'revenue' => $revenue,
            'customer' => $customer
        ]);
    }

    public function products()
    {
        $products = Product::with(['galleries', 'category'])->where('users_id', Auth::user()->id)->get();
        return view('pages.dashboard-products', compact('products'));
    }

    public function products_details($id)
    {
        $product = Product::with(['galleries', 'user', 'category'])->findOrFail($id);
        $categories = Category::all();
        return view('pages.dashboard-products-details', compact('product', 'categories'));
    }

    public function upload_gallery(Request $request)
    {
        $this->validate($request, [
            'photos' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'products_id' => 'required|exists:products,id'
        ]);

        $imageName = time().rand(100,999).".".$request->photos->getClientOriginalExtension();

        $request->photos->move(public_path().'/photos-gallery', $imageName);
        ProductGallery::create([
            'products_id' => $request->products_id,
            'photos' =>$imageName,
        ]);

        alert()->success('Success','Berhasil menambahkan photo !')->persistent('Ok');
        return redirect()->route('dashboard.products.details', $request->products_id);
    }

    public function delete_gallery($id)
    {
        $product = ProductGallery::findOrFail($id);
        $photo = public_path('/photos-gallery/').$product->photos;
        if(file_exists($photo))
        {
            @unlink($photo);
        }
        $product->delete();

        alert()->success('Success','Berhasil menghapus product !')->persistent('Ok');
        return redirect()->back();
    }

    public function products_update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $data = $request->all();

        $data['slug'] = Str::slug($request->name);

        $product->update($data);

        alert()->success('Success','Berhasil memperbarui product !')->persistent('Ok');
        return redirect()->route('dashboard.products');
    }

    public function products_create()
    {
        $categories = Category::all();
        return view('pages.dashboard-products-create', compact('categories'));
    }

    public function products_store(ProductRequest $request)
    {
        $data = $request->all();

        $slug = Str::slug($request->name);
        $product = Product::create([
            'name' => $request->name,
            'users_id' => $request->users_id,
            'categories_id' => $request->categories_id,
            'price' => $request->price,
            'description' => $request->description,
            'slug' => $slug
        ]);

        $imageName = time().rand(100,999).".".$request->photo->getClientOriginalExtension();
        $request->photo->move(public_path().'/photos-gallery', $imageName);
        $gallery = [
            'products_id' => $product->id,
            'photos' => $imageName
        ];

        ProductGallery::create($gallery);

        alert()->success('Success','Berhasil menambahkan product !')->persistent('Ok');
        return redirect()->route('dashboard.products');
    }

    public function transactions()
    {
        $sellTransactions = TransactionDetail::with(['transaction.user', 'product.galleries'])->whereHas('product', function($product){
            $product->where('users_id', Auth::user()->id);
        })->get();

        $buyTransactions = TransactionDetail::with(['transaction.user', 'product.galleries'])->whereHas('transaction', function($transaction){
            $transaction->where('users_id', Auth::user()->id);
        })->get();

        return view('pages.dashboard-transactions', compact('sellTransactions', 'buyTransactions'));
    }

    public function transactions_details($id)
    {
        $transaction = TransactionDetail::with(['transaction.user', 'product.galleries'])->findOrFail($id);

        return view('pages.dashboard-transactions-details', compact('transaction'));
    }

    public function transactions_update(Request $request, $id)
    {
        $data = $request->all();

        $item = TransactionDetail::findOrFail($id);

        $item->update($data);

        return redirect()->route('dashboard.transactions.details', $id);
    }

    public function store_settings()
    {
        $user = Auth::user();
        $categories = Category::all();

        return view('pages.dashboard-store-settings', compact('user', 'categories'));
    }

    public function account_settings()
    {
        $user = Auth::user();
        return view('pages.dashboard-account-settings', compact('user'));
    }

    public function account_settings_update(Request $request, $redirect)
    {
        $data = $request->all();
        $item = Auth::user();

        $item->update($data);

        return redirect()->route($redirect);
    }

}
