@extends('layouts.app')

@section('title')
    Store Category Page
@endsection

@section('content')
<div class="page-content page-categories">
    <section class="store-trend-categories">
      <div class="container">
        <div class="row">
          <div class="col-12" data-aos="fade-up">
            <h5>All Categories</h5>
          </div>
        </div>
        <div class="row">
            @forelse ($categories as $category)
            <div
            class="col-6 col-md-3 col-lg-2"
            data-aos="fade-up"
            data-aos-delay="{{ $i += 100 }}"
            >
                <a href="{{ route('categories.detail', $category->slug) }}" class="component-categories d-block" href="#">
                    <div class="categories-image">
                        <img
                            src="{{ asset('photos/'.$category->photo) }}"
                            alt="Photo Categories"
                            class="w-100"
                        />
                    </div>
                    <p class="categories-text">
                    {{ $category->name }}
                    </p>
                </a>
            </div>
          @empty
            <div class="col-12 text0center py-5" data-aos="fade-up" data-aos-delay="100">No Category Found</div>
          @endforelse
        </div>
      </div>
    </section>
    <section class="store-new-products">
      <div class="container">
        <div class="row">
          <div class="col-12" data-aos="fade-up">
            <h5>All Products</h5>
          </div>
        </div>
        <div class="row">
          @forelse ($products as $product)
            <div
            class="col-6 col-md-4 col-lg-3"
            data-aos="fade-up"
            data-aos-delay="{{ $p += 100 }}"
                >
                <a href="{{ route('detail', $product->slug) }}" class="component-products d-block" href="/details.html">
                    <div class="products-thumbnail">
                        <div
                            class="products-image"
                            style="
                                @if ($product->galleries->count())
                                    background-image: url('{{ asset('photos-gallery/'.$product->galleries->first()->photos) }}')
                                @else
                                    background-image: url('{{ asset('images/400x400.gif') }}')
                                @endif
                            "
                        ></div>
                    </div>
                    <div class="products-text">
                    {{ $product->name }}
                    </div>
                    <div class="products-price">
                    Rp. {{ number_format($product->price) }}
                    </div>
                </a>
            </div>
          @empty
            <div class="col-12 text0center py-5" data-aos="fade-up" data-aos-delay="100">No Product Found</div>
          @endforelse
        </div>
        <div class="mt-5">
            {!! $products->links() !!}
        </div>
      </div>
    </section>
  </div>
@endsection
