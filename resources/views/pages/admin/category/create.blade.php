@extends('layouts.admin')

@section('header')
    <h1>Create New Category</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('category.index') }}">Category</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('category.create') }}">Category Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="nama_category">Nama Category</label>
                            <input type="text" name="name" id="nama_category" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="foto_category">Foto Category</label>
                            <input type="file" name="photo" id="foto_category" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-success px-5">Save Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
