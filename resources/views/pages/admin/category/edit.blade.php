@extends('layouts.admin')

@section('header')
    <h1>Create New Category</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('category.index') }}">Category</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('category.create') }}">Category Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('category.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="nama_category">Nama Category</label>
                            <input type="text" name="name" id="nama_category" class="form-control" value="{{ $category->name }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="foto_category">Foto Category</label>
                            <input type="file" accept="image/*" name="photo" id="imgInp" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="old_photo">Old Photo</label>
                            <br>
                            <img src="{{ asset('photos/'.$category->photo) }}" id="old_photo" class="rounded mt-2 mb-2" style="max-height: 200px;" alt="photo product">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="blah">Preview New Photo</label>
                            <br>
                            <img src="" id="blah" class="rounded mt-2 mb-2" style="max-height: 200px;" alt="new photo not selected">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-warning px-5">Update Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script>
        imgInp.onchange = evt => {
            const [file] = imgInp.files
            if (file) {
                blah.src = URL.createObjectURL(file)
            }
        }
    </script>
@endpush
