@extends('layouts.admin')

@section('header')
    <h1>Create New Transaction</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('transaction.index') }}">Transaction</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('transaction.update', $transaction->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="transaction_status">Transaction Status</label>
                            <select name="transaction_status" id="transaction_status" class="form-control">
                                <option value="PENDING" @if($transaction->transaction_status == 'PENDING') selected @endif>Pending</option>
                                <option value="SHIPPING" @if($transaction->transaction_status == 'SHIPPING') selected @endif>Shipping</option>
                                <option value="SUCCESS" @if($transaction->transaction_status == 'SUCCESS') selected @endif>Success</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="total_price">Total Price</label>
                            <input type="number" name="total_price" id="total_price" class="form-control" value="{{ $transaction->total_price }}" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-warning px-5">Update Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endpush
