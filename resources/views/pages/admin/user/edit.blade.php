@extends('layouts.admin')

@section('header')
    <h1>Create New User</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('user.create') }}">User Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('user.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="nama_user">Nama User</label>
                            <input type="text" name="name" id="nama_user" class="form-control" value="{{ $user->name }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="email">Email User</label>
                            <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="password">Password User</label>
                            <input type="password" name="password" id="password" class="form-control">
                            <small class="text-danger">Kosongkan jika tidak ingin mengganti password !</small>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="roles">Roles User</label>
                            <select name="roles" id="roles" class="form-control">
                                <option value="ADMIN" @if($user->roles == "ADMIN") selected @endif>Admin</option>
                                <option value="USER" @if($user->roles == "USER") selected @endif>User</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-warning px-5">Update Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
