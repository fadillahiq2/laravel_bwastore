@extends('layouts.admin')

@section('header')
    <h1>Create New Product</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('product.index') }}">Product</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('product.create') }}">Product Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('product.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="nama_product">Nama Product</label>
                            <input type="text" name="name" id="nama_product" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="pemilik">Pemilik Product</label>
                            <select name="users_id" id="pemilik" class="form-control">
                                <option value="" selected disabled>Pilih Pemilik</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="kategori">Kategori Product</label>
                            <select name="categories_id" id="kategori" class="form-control">
                                <option value="" selected disabled>Pilih Kategori</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="harga">Harga Product</label>
                            <input type="number" name="price" id="harga" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="deskripsi">Deskripsi Product</label>
                            <textarea name="description" id="editor" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-success px-5">Save Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endpush
