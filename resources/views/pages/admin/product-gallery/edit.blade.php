@extends('layouts.admin')

@section('header')
    <h1>Create New Product Gallery</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('product-gallery.index') }}">Product Gallery</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('product-gallery.create') }}">Product Gallery Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('product-gallery.update', $product_gallery->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="products">Product</label>
                            <select name="products_id" id="products" class="form-control">
                                <option value="" selected disabled>Pilih Product</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}" @if($product->id == $product_gallery->product->id) selected @endif>{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="photos">Photo Product</label>
                            <input type="file" name="photos" id="photos" class="form-control" onchange="loadFile(event)" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="old_photo">Old Photo</label>
                            <br>
                            <img src="{{ asset('photos-gallery/'.$product_gallery->photos) }}" id="old_photo" class="rounded mt-2 mb-2" style="max-height: 200px;" alt="photo product">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="preview_photo">Preview New Photo</label>
                            <br>
                            <img src="" id="preview_photo" class="rounded mt-2 mb-2" style="max-height: 200px;" alt="new photo not selected">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-warning px-5">Update Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script>
        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('preview_photo');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endpush
