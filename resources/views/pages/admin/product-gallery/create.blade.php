@extends('layouts.admin')

@section('header')
    <h1>Create New Product Gallery</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ route('product-gallery.index') }}">Product Gallery</a></div>
        <div class="breadcrumb-item active"><a href="{{ route('product-gallery.create') }}">Product Gallery Create</a></div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-body">
            <form action="{{ route('product-gallery.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="products">Product</label>
                            <select name="products_id" id="products" class="form-control">
                                <option value="" selected disabled>Pilih Product</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="photos">Photo Product</label>
                            <input type="file" name="photos" id="photos" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" id="btn-submit" class="btn btn-success px-5">Save Now</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endpush
